describe('Test menu and loading of data', function() {
	beforeEach(function() {
		browser.get('/#/');
	});


	it('open app, load data, click on trinity, load menu', function() {
		// splash screen loaded
		expect(browser.getLocationAbsUrl()).toMatch('/splash');
		// wait for data to load
		browser.driver.sleep(20000);

		// data is loaded and user has been redirected
		expect(browser.getLocationAbsUrl()).toMatch('/app/food_data/');
		// open the menu
		var menuToggleButton = element(by.css('ion-side-menus [nav-bar="active"] [menu-toggle="left"]'));
		menuToggleButton.click();
		// click on trinity
		var trinitylink = element(by.css('ion-side-menu ion-list ion-item:nth-child(1)'));
		// wait for button to become 'clickable' (menu is opened)
		var EC = protractor.ExpectedConditions;
		var isTrinityLinkClickable = EC.elementToBeClickable(trinitylink);
		browser.wait(isTrinityLinkClickable, 5000);
		trinitylink.click();

		var getFirstDayMenu = element(by.css('ion-side-menu ion-list ion-item'))

		browser.driver.sleep(1000);
		element.all(by.css('.item.item-divider')).get(0).getText().then(function(text) {
			expect(text).toMatch(/^[a-zA-Z]+, [a-zA-Z]+ [0-9]{1,2}, [0-9]{4}/);
		})
		// pause before finishing
		browser.driver.sleep(1000);
	});

	// it('application should open on college previously selected', function() {
	// 	// write code here
	// 	browser.close();
	// 	browser.get('/#/');
	// 	browser.driver.sleep(20000);
	// 	expect(browser.getLocationAbsUrl()).toMatch('/app/food_data/');
	// 	element.all(by.css('.item.item-divider')).get(0).getText().then(function(text) {
	// 		expect("Saturday, December 5, 2015").toMatch(/^[a-zA-Z]+, [a-zA-Z]+ [0-9]{1,2}, [0-9]{4}/);
	// 	})
	// });

});


