// Ionic Starter App

var localCollegeDB = new PouchDB("colleges");
var remoteCollegeDB = new PouchDB("http://foodapp.cloudns.club:5984/colleges");


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('listColleges', {
    url : '/colleges',
    templateUrl: 'templates/colleges.html',
    controller: 'listCollegesCtrl',
  })

  $urlRouterProvider.otherwise('/colleges');
})
.controller('listCollegesCtrl', function($scope) {
  $scope.test = ["one", "two", "three"]; 
  $scope.errorlog = "running replication";
  $scope.adapter = localCollegeDB.adapter;
  $scope.dbinfo = "";
  //PouchDB.debug.enable('*');
  localCollegeDB.info().then(function(result) {
    $scope.dbinfo = result;
    $scope.$apply();
  });
  
  // first replicate data from remote database to the local database
  localCollegeDB.replicate.from(remoteCollegeDB, {})
  .on('error', function(info) {
    console.log(info);
    $scope.errorlog = "There was an error:" + info;
    $scope.$apply();
  })
  .on('complete', function(info) {
    // once replication from remote to local is complete load data from the local db
    localCollegeDB.allDocs({
      include_docs:true,
      attachments:true
    }).then(function(result) {
      // extract the 'college' property from the documents returned, add them to an array
      // and resolve the promise
      result_array = []
      for (var i = 0; i < result["rows"].length; i++) {
        result_array[i] = result["rows"][i]["doc"]["college"];
      }
      // log output
      console.log(result_array)

      $scope.colleges = result_array
      $scope.errorlog = "completed, no errors";
      // reload the scope
      $scope.$apply();
    })
  })
})